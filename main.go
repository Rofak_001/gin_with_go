package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"time"
)

type Employee struct {
	Name string
	Age string
	Sex string
	Position string
}
func main() {
	r:=gin.Default()
	r.POST("/employee",createEmp)
	r.PUT("/employee/:id",updateEmp)
	r.DELETE("/employee/:id",deleteEmp)
	r.GET("/employee",getAll)
	r.GET("/employee/:id",getOne)
	r.Run()
}
func getConnection() (*mongo.Client,context.Context) {
	client,err:=mongo.NewClient(options.Client().ApplyURI("mongodb+srv://test:test@cluster0.keplo.mongodb.net/<dbname>?retryWrites=true&w=majority"))
	if err!=nil{
		log.Fatal(err)
	}
	ctx,_:=context.WithTimeout(context.Background(),10*time.Second)
	err =client.Connect(ctx)
	if err!=nil{
		log.Fatal(err)
	}
	return client,ctx
}

func createEmp(c *gin.Context)  {
	client,ctx:=getConnection()
	var emp Employee
	err :=c.ShouldBind(&emp)
	if err!=nil{
		log.Fatal(err)
		c.JSON(http.StatusBadRequest,gin.H{"message":err})
	}
	defer client.Disconnect(ctx)
	result, err :=client.Database("Employee").Collection("Employee").InsertOne(ctx,emp)
	if err!=nil{
		log.Fatal(err)
		c.JSON(http.StatusInternalServerError,gin.H{"message":err})
	}
	id:=result.InsertedID.(primitive.ObjectID)
	c.JSON(http.StatusOK,gin.H{"id":id})
}
func updateEmp(c *gin.Context)  {
	client,ctx:=getConnection()
	var emp Employee
	err:=c.ShouldBind(&emp)
	if err!=nil{
		log.Fatal(err)
		c.JSON(http.StatusBadRequest,gin.H{"message":err})
	}
	getId:=c.Params.ByName("id")
	id,_:=primitive.ObjectIDFromHex(getId)
	result, err := client.Database("Employee").Collection("Employee").UpdateOne(
		ctx,
		bson.M{"_id": id},
		bson.D{{"$set", emp}})
	if err!=nil{
		log.Fatal(err)
		c.JSON(http.StatusNotFound,gin.H{"Message":"Not Found"})
	}
	if  result.MatchedCount>=1{
		c.JSON(http.StatusOK,gin.H{"Message":"Updated Successfully!"})
	}
}
func deleteEmp(c *gin.Context)  {
	client,ctx:=getConnection()
	getId:=c.Params.ByName("id")
	id,_:=primitive.ObjectIDFromHex(getId)
	resul,err:=client.Database("Employee").Collection("Employee").DeleteOne(
			ctx,
			bson.M{"_id":id})
	if err!=nil{
		log.Fatal(err)
	}
	if resul.DeletedCount>=1{
		c.JSON(http.StatusOK,gin.H{"Message":"Deleted Successfully!"})
	}
	fmt.Printf("DeleteOne removed %v doucment",resul.DeletedCount)
}
func getAll(c *gin.Context)  {
	client,ctx:=getConnection()
	emp,err:=client.Database("Employee").Collection("Employee").Find(ctx,bson.M{})
	if err !=nil{
		log.Fatal(err)
	}
	var emps []bson.M
	if err=emp.All(ctx,&emps); err !=nil{
		log.Fatal(err)
	}
	c.JSON(http.StatusOK,gin.H{"Employee":emps})
}
func getOne(c *gin.Context)  {
	client,ctx:=getConnection()
	getId:=c.Params.ByName("id")
	id, _ :=primitive.ObjectIDFromHex(getId)
	var podcast bson.M
	err:=client.Database("Employee").Collection("Employee").FindOne(ctx,bson.M{"_id":id}).Decode(&podcast)
	if err !=nil{
		log.Fatal(err)
	}
	c.JSON(http.StatusOK,gin.H{"employee":podcast})
}